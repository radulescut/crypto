# TP2 : décomposition d'un entier en produit de facteurs premiers
## Exercice 2 :
1. f(n) vaut p1.
2. a.
2^(beta) <= beta < 2^(beta+1)

b.
racine(2^(beta)) <= racine(beta) < racine(2^(beta+1))
<=>

2^(beta/2) <= racine(beta) < 2^((beta+1)/2)

c.
minorant = 2^(beta/2)