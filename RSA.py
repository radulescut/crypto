import math

import usuel
import random
import sympy


def random_prime(b: int) -> int:
    a = random.randint(2 ** (b - 1), 2 ** b - 1)
    while not random_prime_test(a):
        a = random.randint(2 ** (b - 1), 2 ** b - 1)
    return a


def random_prime_test(b: int) -> bool:
    if usuel.exponentiation_modulaire(2, b - 1, b) == 1:
        return True
    return False


def step1(b: int) -> tuple[int, int]:
    # return sympy.randprime(2 ** (b - 1), 2 ** b - 1), sympy.randprime(2 ** (b - 1), 2 ** b - 1)
    return random_prime(b), random_prime(b)


def step2(b: int, p: int, q: int) -> tuple[int, int]:
    phi = (p - 1) * (q - 1)
    e = random.randint(1, phi)
    while usuel.euclide(e, phi) != 1:
        e = random.randint(1, phi)
    d = usuel.euclide_etendu(e, phi)[1]
    while d < 0:
        e = random.randint(1, phi)
        while usuel.euclide(e, phi) != 1:
            e = random.randint(1, phi)
        d = usuel.euclide_etendu(e, phi)[1]
    return e, d


def keygen(b) -> tuple[tuple[int, int], tuple[int, int]]:
    p, q = step1(b)
    e, d = step2(b, p, q)
    return (p * q, e), (p * q, d)


def chiffre(m: int, pk: tuple[int, int]) -> int:
    return usuel.exponentiation_modulaire(m, pk[1], pk[0])


def dechiffre(c: int, sk: tuple[int, int]) -> int:
    return usuel.exponentiation_modulaire(c, sk[1], sk[0])


def main():
    pk, sk = keygen(100)
    print(pk, sk)
    message = 69
    x = chiffre(message, pk)
    print(dechiffre(x, sk))


if __name__ == "__main__":
    main()
