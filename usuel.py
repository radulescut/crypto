def euclide(a: int, b: int) -> int:
    if b == 0:
        return a
    return euclide(b, a % b)


def euclide_etendu(a: int, b: int) -> tuple[int, int, int]:
    if b == 0:
        return a, 1, 0

    d_p, u_p, v_p = euclide_etendu(b, a % b)

    return d_p, v_p, u_p - ((a // b) * v_p)


def exponentiation_modulaire(a: int, b: int, n: int) -> int:
    d = 1
    bin_b = bin(b)[2:]
    for i in range(len(bin_b)):
        d = (d * d) % n
        if bin_b[i] == '1':
            d = (d * a) % n
    return d




def puiss_nul(a, b, n):
    i = 0
    x = 1
    while i < b:
        x = (a * x) % n
        i = i + 1
    return x


def main():
    # assert ((5 ** 5) % 3 == exponentiation_modulaire(5, 5, 3))
    print(w(3, 7))


def w(x: int, n: int) -> int:
    k = 1
    while (x ** k) % n != 1:
        print((x ** k) % n)
        k = k + 1
    return k


if __name__ == "__main__":
    main()
