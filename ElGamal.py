import random

import usuel


def random_prime(b: int) -> int:
    a = random.randint(2 ** (b - 1), 2 ** b - 1)
    while not random_prime_test(a):
        a = random.randint(2 ** (b - 1), 2 ** b - 1)
    return a


def random_prime_test(b: int) -> bool:
    if usuel.exponentiation_modulaire(2, b - 1, b) == 1:
        return True
    return False


def keygen(b: int) -> tuple[tuple[int, int, int, int], int]:
    p = random_prime(b)
    a = random.randint(0, p - 1)
    q = p - 1
    x = random.randint(0, q - 1)
    h = usuel.exponentiation_modulaire(a, x, p)
    return (p, q, a, h), x


def encrypt(m: int, pk: tuple[int, int, int, int]) -> tuple[int, int]:
    r = random.randint(0, pk[1])
    c1 = usuel.exponentiation_modulaire(pk[2], r, pk[0])
    c2 = ((m % pk[0]) * usuel.exponentiation_modulaire(pk[3], r, pk[0])) % pk[0]
    return c1, c2


def decrypt(c: tuple[int, int], sk, pk) -> int:
    return ((c[1] % pk[0]) * usuel.exponentiation_modulaire(c[0], pk[1] - sk, pk[0])) % pk[0]


def main():
    pk, sk = keygen(1000)
    print(pk, sk)
    message = 69
    c = encrypt(message, pk)
    print(c)
    print(decrypt(c, sk, pk))


if __name__ == '__main__':
    main()
