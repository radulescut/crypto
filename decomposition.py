from math import *


def pdiviseur(n: int) -> int:
    if n % 2 == 0:
        return 2
    for k in range(3, int(sqrt(n)) + 1, 2):
        if n % k == 0:
            return k
    return n


def liste(n) -> list:
    if n == 1:
        return []
    p = pdiviseur(n)
    return [pdiviseur(n)] + liste(n / p)


def main():
    print(pdiviseur(10))
    print(liste(48))


if __name__ == '__main__':
    main()
