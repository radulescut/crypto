import random
from usuel import euclide_etendu


def euclide(a: int, b: int) -> int:
    """
    :param a: an integer
    :param b: a second integer
    :return: a and b's pgcd
    """
    if b == 0:
        return a
    return euclide(b, a % b)


def euclide_etendu_rev(a: int, b: int) -> tuple[int, int, int]:
    """
    :param a: an integer
    :param b: a second integer
    :return: a tuple containing three members such as secondMember * a+ thrid member * b = first parameter = pgcd of a and b
    """
    if b==0:
        return a,1,0
    d,u,v = euclide_etendu(b,a%b)
    return d,v,u-(a//b)*v


def expomod(a: int, b: int, n: int) -> int:
    """
    :param a: the power base
    :param b: the exponent
    :param n: the modulus
    :return: the result of a^b % n
    """
    d = 1
    b_bin = bin(b)[2:]
    for k in range(len(b_bin)):
        d = (d * d)%n
        if b_bin[k] == '1':
            d = (d*a)%n
    return d


def random_test_pow(start: int, end: int):
    a = random.randint(start, end)
    b = random.randint(start, end)
    n = random.randint(start, end)
    print(f'{a}^{b}%{n} = {expomod(a, b, n)}')
    assert ((a ** b) % n == expomod(a, b, n))


def random_test_ee(start: int, end: int):
    a = random.randint(start, end)
    b = random.randint(start, end)
    d, u, v = euclide_etendu(a, b)
    print(f'{a}^{b} = {a}*{u}+{b}*{v} = {d}')
    assert (euclide_etendu(a, b) == euclide_etendu_rev(a, b))


def main():
    assert (euclide(6, 3) == 3)
    assert (euclide(10, 6) == 2)
    assert (euclide(17, 6) == 1)
    random_test_pow(3, 50)
    random_test_pow(3, 50)
    random_test_pow(3, 50)
    random_test_pow(3, 5000)
    random_test_pow(3, 5000)
    random_test_ee(3,50)
    random_test_ee(3,50)
    random_test_ee(3,5000)
    random_test_ee(3,5000)


if __name__ == "__main__":
    main()
